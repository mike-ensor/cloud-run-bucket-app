# Overview

This is a simple web-server based container application that lists out the contents of a Google Cloud Storage bucket. The project is intended for testing and demonstration purposes only and should not be used for any sensitive workloads or used in a production-like environment.

>NOTE: This is NOT an official Google managed or maintained project.

## Environment Variables

There are two primary environment variables that are exposed and can be used to change output.

* `ENVIRONMENT` - Optional variable used to denote an environment
    * Setting this value will display the value in the output. Primary use is to show configuration changes between runs/deployments of container
    * Default is empty string and not displayed
* `BUCKET` will display the contents of the given GCS bucket IF no query parameter is provided
    * Default value is empty and will not query a bucket contents
    * NOTE: A "bucket" query parameter take precedence over this value, so ONLY if the query parameter is empty, will this variable be used. If neither are specified, then no bucket query is triggered

## Running Locally

### Prerequisites
* Ensure golang v1.14+ is installed and configured
* Using `bash` or `zsh` shell (or have advanced knowledge to convert bash-like commands to shell of preference)
* Authenticated with `gcloud`
    * `gcloud init` to authenticate
    * `gcloud config list account --format "value(core.account)"` to verify active account
* Have read+ access to a Google Cloud Storage bucket for the authenticated user

## Query Bucket

### Run App
```bash
cd src/
go run .
# When done, use "ctrl+c" to exit app
cd .. # back to repo root
```
### View Bucket
1. Open a browser to: `http://localhost:8080/?bucket=`
1. Specify bucket visible for the authenticated `gcloud` user (see prerequisites if unable to determine active user account)
    * `http://localhost:8080/?bucket=<bucket-name>`

# Using Cloud Run

## Variables Used

The following variables should be defined and set in the shell context. Over the following series of commands, the variables will reference these variables:

```bash
export PROJECT_ID=XXXX # Insert the Google Project ID here
export SERVICE=bucket-list-app # Or choose a new name for the app
export REGION=XXXX # Region to deploy
export TARGET_BUCKET=XXXX # Bucket to query as a default/backup (optional)
```

## Build Container
1. Create the docker image

```bash
gcloud builds submit --substitutions=_PROJECT_ID=<INSERT PROJECT ID>
```

### Sample Output
```text
...
...
PUSH
Pushing gcr.io/XXXXX/cloud-run-bucket-app
The push refers to repository [gcr.io/XXXXX/cloud-run-bucket-app]
5f9ef59aace9: Preparing
236f427c513a: Preparing
79d541cda6cb: Preparing
79d541cda6cb: Layer already exists
236f427c513a: Layer already exists
5f9ef59aace9: Pushed
latest: digest: sha256:0b7f0c0333454d3b34512df171b5ab45cbba6436e4ee07197f2e79575affa166 size: 949
DONE
-------------------------------------------------------------------------------------------------------

ID                                    CREATE_TIME                DURATION  SOURCE                                                                                    IMAGES                                            STATUS
b2b46222-561d-42c4-ac51-dac844c10b30  2020-09-06T19:48:02+00:00  1M16S     gs://XXXXXXX_cloudbuild/source/2594421119.346188-4631b45f042bc32ca6ffd2771d172077.tgz  gcr.io/XXXXX/cloud-run-bucket-app (+1 more)  SUCCESS
```

## Create a Cloud Run Service (Deploy)

Create a new Cloud Run service. The result will be a URL that can be opened in a browser.

>Note: This command is below is a simplistic deployment. See [Run Deploy](https://cloud.google.com/sdk/gcloud/reference/run/deploy) for more details.

```bash
gcloud run deploy ${SERVICE} \
    --platform managed \
    --region ${REGION} \
    --allow-unauthenticated \
    --image gcr.io/${PROJECT_ID}/cloud-run-bucket-app
```
>Note: Image name is defined in the `cloudbuild.yaml` file by default. If a new name is desired, pass in a name to the cloud build run using the `substitutions` as done with the `${PROJECT_ID}`

### Sample Output
```text
$ gcloud run deploy ${SERVICE} \
>     --platform managed \
>     --region ${REGION} \
>     --allow-unauthenticated \
>     --image gcr.io/${PROJECT_ID}/cloud-run-bucket-app
Deploying container to Cloud Run service [********-******] in project [********] region [**-********]
✓ Deploying... Done.
  ✓ Creating Revision...
  ✓ Routing traffic...
  ✓ Setting IAM Policy...
Done.
Service [********-******] revision [********-******-00002-zib] has been deployed and is serving 100 percent of traffic at https://********-******-t47kuibcca-uw.a.run.app
```

## Accessing Service

The completion of the deployment will result in a URL that can be opened in a browser. In this example, the format is:

`https://<service-name>-<random-charaters>.a.run.app`

## Testing

1. Open the URL without any query parameters
    * Should see a nearly empty page with a browser title
1. Add a query parameter (?bucket=<some-bucket-name-open-to-default-compute-service-account>)
    * Should see a list of bucket contents (if the query is less than 10s, else see an error)

## Adding Environment Variables

>Note: Optional

The previous deployment steps do not include environment variables. To set an environment variables on the Cloud Run instance as listed below. Note, the "`BUCKET`" variable is used as a default bucket to query if there is no query parameter assigned.

```bash
gcloud run services update ${SERVICE} \
     --update-env-vars \
     BUCKET=${TARGET_BUCKET},ENVIRONMENT=Development
```