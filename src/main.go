package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	_ "path"
	"strings"

	"context"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

func main() {
	handler := GetHTTPHandlers()
	/* #nosec */
	http.ListenAndServe(fmt.Sprintf("0.0.0.0:8080"), &handler)
}

// GetHTTPHandlers sets up and runs the main http server
func GetHTTPHandlers() (handlers http.ServeMux) {
	handler := new(http.ServeMux)
	handler.HandleFunc("/", SayHelloHandler)
	handler.HandleFunc("/_health", HealthCheckHandler)

	return *handler
}

// SayHelloHandler handles a response
func SayHelloHandler(w http.ResponseWriter, r *http.Request) {
	var output strings.Builder
	var bucket string

	currentEnvironment := os.Getenv("ENVIRONMENT")
	// Get query parameter value
	bucketQueryParam, ok := r.URL.Query()["bucket"]

	// if bucket is defined as a parameter, the use that
	if !ok || len(bucketQueryParam[0]) < 1 {
		// default to BUCKET environment variable
		bucket = os.Getenv("BUCKET")
	} else {
		bucket = string(bucketQueryParam[0])
	}

	w.Header().Set("Content-Type", "text/html")

	output.WriteString(fmt.Sprintf("<html><head><title>Container Contents %s</title></head><body>", bucket))

	if len(currentEnvironment) > 0 {
		output.WriteString(fmt.Sprintf("<h2>Current Environment: %s</h2>", currentEnvironment))
	}

	output.WriteString("<h1>Contents of a bucket</h1>")

	if len(bucket) > 0 {
		bucketList, err := htmlListFiles(bucket)

		if err != nil {
			output.WriteString(fmt.Sprintf("<h3>Error accessing bucket '%s'<h3>", bucket))
			output.WriteString(fmt.Sprintf("<p>Err: %v</p>", err))
		} else {
			output.WriteString(fmt.Sprintf("<h3>Bucket '%s' Contents</h3>", bucket))
			output.WriteString(bucketList.String())
		}
	} else {
		output.WriteString("No bucket value set. Use '?bucket=<bucketname>' or set 'BUCKET' environment variable to list contents of a bucket")
	}

	output.WriteString("</body><html>")

	// write output to stream
	fmt.Fprintf(w, output.String())
}

// HealthCheckHandler responds with a mocked "ok" (real prod app should do some work here)
func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")

	_, err := io.WriteString(w, `{"alive": true}`)
	if err != nil {
		// fmt.Errorf("Unable to write to response with error: %w", err)
		log.Fatal(err)
	}
}

// htmlListFiles lists objects within specified bucket.
func htmlListFiles(bucket string) (strings.Builder, error) {
	// bucket := "bucket-name"
	var output strings.Builder

	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		return output, fmt.Errorf("storage.NewClient: %v", err)
	}
	defer client.Close()

	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()

	it := client.Bucket(bucket).Objects(ctx, nil)
	output.WriteString("<ul>")
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return output, fmt.Errorf("%v", err)
		}
		output.WriteString(fmt.Sprintf("<li>%s</li>", attrs.Name))
	}
	output.WriteString("</ul>")
	return output, err
}
