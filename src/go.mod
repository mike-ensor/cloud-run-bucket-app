module google.com/hello-world

go 1.14

require (
	cloud.google.com/go/storage v1.11.0
	google.golang.org/api v0.31.0
	rsc.io/quote v1.5.2
)
