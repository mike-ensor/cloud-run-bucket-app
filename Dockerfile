# Start by building the application.
FROM golang:1.14-buster as build

WORKDIR /src/
COPY src/ /src

RUN go get -d -v ./...

RUN go build -o /go/bin/app

# Copy to distroless base
FROM gcr.io/distroless/base-debian10
COPY --from=build /go/bin/app /
CMD ["/app"]